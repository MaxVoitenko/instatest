package com.example.testinsta.Ui;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.Post;
import com.example.testinsta.R;
import com.github.siyamed.shapeimageview.RoundedImageView;

import java.text.MessageFormat;
import java.util.List;

public class Adapeter extends RecyclerView.Adapter <Adapeter.Holder> {

    private List<Post> items;
    private Context context;

    public Adapeter(List<Post> items, Context context) {
        this.items = items;
        this.context = context;
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new Holder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_recycler, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int position) {
        Post post = items.get(holder.getAdapterPosition());

        holder.nameTxt.setText(post.getName());
        holder.cityTxt.setText(post.getCity());
        holder.commentTxt.setText(post.getComment());
        List<String> likers = post.getArray();
        if(likers.size()>=2){
        holder.likeByTxt.setText(MessageFormat.format("liked by {0}, {1}, {2} and {3} other", likers.get(0),likers.get(1),
                likers.get(2),String.valueOf(likers.size()-2)));
        }else{
            String idList = likers.toString();
            String csv = idList.substring(1, idList.length() - 1).replace(", ", ",");
            holder.likeByTxt.setText(csv);
        }
        int imageId = context.getResources().getIdentifier(post.getLinkPostPic(), "drawable", context.getPackageName());
        holder.mainImage.setImageResource(imageId);
        imageId = context.getResources().getIdentifier(post.getLinkProfilePic(), "drawable", context.getPackageName());
        holder.profileImage.setImageResource(imageId);

        View.OnClickListener clickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callbackClick.clickAdapter(v);
            }
        };
        holder.sendBtn.setOnClickListener(clickListener);
        holder.commentBtn.setOnClickListener(clickListener);
        holder.likeBtn.setOnClickListener(clickListener);
        holder.menuBtn.setOnClickListener(clickListener);
        holder.saveBtn.setOnClickListener(clickListener);

    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    class Holder extends RecyclerView.ViewHolder{

        RoundedImageView profileImage;
        TextView nameTxt;
        TextView cityTxt;
        ImageButton menuBtn;
        ImageView mainImage;
        ImageButton likeBtn;
        ImageButton commentBtn;
        ImageButton saveBtn;
        ImageButton sendBtn;
        TextView likeByTxt;
        TextView commentTxt;


        public Holder(View itemView) {
            super(itemView);
            profileImage = itemView.findViewById(R.id.profileImage);
            nameTxt = itemView.findViewById(R.id.nameTxt);
            cityTxt = itemView.findViewById(R.id.cityTxt);
            menuBtn = itemView.findViewById(R.id.menuBtn);
            mainImage = itemView.findViewById(R.id.mainImage);
            likeBtn = itemView.findViewById(R.id.likeBtn);
            commentBtn = itemView.findViewById(R.id.commentBtn);
            saveBtn = itemView.findViewById(R.id.saveBtn);
            sendBtn = itemView.findViewById(R.id.sendBt);
            likeByTxt = itemView.findViewById(R.id.LikeByTxt);
            commentTxt = itemView.findViewById(R.id.commentTxt);
        }
    }

    private Callback callbackClick;
    public interface Callback{ void clickAdapter(View v);}
    public void setOnClickAdapter(Callback callback){ this.callbackClick = callback; }
}

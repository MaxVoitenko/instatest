package com.example.testinsta.Providers;

import android.content.Context;
import com.example.Post;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.util.List;

public class NetPostProvider {

    private Context context;

    public NetPostProvider(Context context) {
        this.context = context;
    }

    public List<Post> getAll(){
        Gson gson = new Gson();
        Type collectionType = new TypeToken<List<Post>>(){}.getType();
        List<Post> posts = (List<Post>) new Gson().fromJson( getJsonFile() , collectionType);
        return posts;
    }

private String getJsonFile() {
    String text= "-1";
        try {
            InputStream istream = context.getAssets().open("data.txt");
            int size = istream.available();
            byte[] buffer = new byte[size];
            istream.read(buffer);
            text = new String(buffer);
            istream.close();
    } catch (FileNotFoundException e) {
        e.printStackTrace();
    } catch (IOException e) {
        e.printStackTrace();
    }finally {
            return text;
        }
}
}
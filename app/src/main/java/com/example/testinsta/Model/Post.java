
package com.example;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Post {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("comment")
    @Expose
    private String comment;
    @SerializedName("array")
    @Expose
    private List<String> array = null;
    @SerializedName("linkProfilePic")
    @Expose
    private String linkProfilePic;
    @SerializedName("linkPostPic")
    @Expose
    private String linkPostPic;

    /**
     * No args constructor for use in serialization
     * 
     */
    public Post() {
    }

    /**
     * 
     * @param linkProfilePic
     * @param name
     * @param comment
     * @param linkPostPic
     * @param array
     * @param city
     */
    public Post(String name, String city, String comment, List<String> array, String linkProfilePic, String linkPostPic) {
        super();
        this.name = name;
        this.city = city;
        this.comment = comment;
        this.array = array;
        this.linkProfilePic = linkProfilePic;
        this.linkPostPic = linkPostPic;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public List<String> getArray() {
        return array;
    }

    public void setArray(List<String> array) {
        this.array = array;
    }

    public String getLinkProfilePic() {
        return linkProfilePic;
    }

    public void setLinkProfilePic(String linkProfilePic) {
        this.linkProfilePic = linkProfilePic;
    }

    public String getLinkPostPic() {
        return linkPostPic;
    }

    public void setLinkPostPic(String linkPostPic) {
        this.linkPostPic = linkPostPic;
    }

}

package com.example.testinsta.Ui;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.example.Post;
import com.example.testinsta.Providers.NetPostProvider;
import com.example.testinsta.R;

import java.util.List;

public class MainActivity extends AppCompatActivity implements Adapeter.Callback {


    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        recyclerView  = findViewById(R.id.recyclerView);
        NetPostProvider netPostProvider = new NetPostProvider(this);
        List<Post> items = netPostProvider.getAll();
        Adapeter adapeter = new Adapeter(items, this);
        adapeter.setOnClickAdapter(this);
        recyclerView.setAdapter(adapeter);
        adapeter.notifyDataSetChanged();
    }

    @Override
    public void clickAdapter(View v) {
        switch (v.getId()){
            case R.id.sendBt:
                Toast.makeText(this, "send", Toast.LENGTH_SHORT).show();
                break;
            case R.id.commentBtn:
                Toast.makeText(this, "comment", Toast.LENGTH_SHORT).show();
                break;
            case R.id.likeBtn:
                Toast.makeText(this, "like", Toast.LENGTH_SHORT).show();
                break;
            case R.id.menuBtn:
                Toast.makeText(this, "menu", Toast.LENGTH_SHORT).show();
                break;
            case R.id.saveBtn:
                Toast.makeText(this, "save pic", Toast.LENGTH_SHORT).show();
                break;
        }
    }
}
